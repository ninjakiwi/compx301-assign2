//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 Unpacker (lib)
//!
//! Unpacks a stream of bytes to LZ78 pairs, one pair per line.
//! Can take input from `LZpack.rs`.
//! (Same output as `LZencode.rs`)
//! The unpack function is separate to allow easier testing
//! (used as a lib).

use crate::helpers;
use std::io::{Write, Read, BufWriter, BufReader};

/// Reads the stream of bytes and outputs as LZ78 pairs line by line
///
/// ### Arguments
///
/// - **input** - A object that implements the read trait; data to be unpacked.
/// - **output** - A object that implements the write trait; used as output.
/// - **arguments** - `ProgramArguments` from `helpers.rs`.
///
/// ### Returns
///
/// The function returns no value `(())`, but the unpacked data (in intermediate format) is written to **output**.
pub fn unpack<R: Read, W: Write>(input: R, output: W, arguments: &helpers::ProgramArguments) {

	// Read with 1 KB buffer
	let mut buf_reader = BufReader::with_capacity(1000, input);
	let mut buf_writer = BufWriter::with_capacity(1000, output);


	let max_trie_size = if arguments.memory_restricted { // Read the leading 8 bytes for the mem limit
		let mut max_trie_size_bytes = [0u8; 8];
		buf_reader.read(&mut max_trie_size_bytes).expect("No leading 8 bytes, maybe remove -m flag?");
		let max_trie_size = helpers::bytes_to_u64(&max_trie_size_bytes);
		if arguments.ascii_output {
			// Write out the max_trie_size_data as ascii int
			buf_writer.write(max_trie_size.to_string().as_bytes()).unwrap();
			buf_writer.write(b"\n").unwrap();
		} else {
			// Write out the max_trie_size_data as-is
			buf_writer.write(&max_trie_size_bytes).unwrap();
		}
		max_trie_size
	} else { // Otherwise use default size.
		std::u64::MAX / 2
	};

	let pairs =
		UnpackMismatchPair::new(
			buf_reader.bytes().map(|c| c.expect("Failed to read")),
			max_trie_size);
	for (pattern_idx, mismatch) in pairs {
		// write pattern and mismatch to output
		helpers::write_pair(&mut buf_writer, pattern_idx, mismatch, arguments.ascii_output);
	}
}

/// This struct provides a LZ78 iterator over a byte iterator
/// where the bytes contains the compressed pairs from `LZpack.rs`
pub struct UnpackMismatchPair<I: Iterator<Item=u8>> {
	source: I,
	counter: u64,
	shift: u8,
	remainder: u8,
	max_trie_size: u64,
}

impl<I: Iterator<Item=u8>> UnpackMismatchPair<I> {
	pub fn new(source: I, max_trie_size: u64) -> UnpackMismatchPair<I> {
		UnpackMismatchPair {
			source,
			counter: 0,
			shift: 0,
			remainder: 0,
			max_trie_size,
		}
	}
}

impl<I: Iterator<Item=u8>> Iterator for UnpackMismatchPair<I> {
	type Item = (u64, u8);
	fn next(&mut self) -> Option<(u64, u8)> {
		// Calculate the maximum number of bits needed
		let mut num_of_bits_needed = ((self.counter.min(self.max_trie_size) + 1) as f64).log2().ceil() as u64;
		self.counter += 1;
		if num_of_bits_needed <= self.shift as u64 { // The remainder has more stuff than pattern_idx needs

			// Move the number of bits needed from the remainder to pattern_idx
			let pattern_idx = if num_of_bits_needed == 0 { 0 } else { (self.remainder >> (8 - num_of_bits_needed as u8)) as u64 };

			self.shift -= num_of_bits_needed as u8; // Move shift to the correct position.

			// If pattern_idx used up everything, mismatch will be empty, otherwise put the rest into mismatch.
			let mut mismatch = if self.shift == 0 { 0 } else { self.remainder << num_of_bits_needed }; // self.shift always equals 0 when num_of_bits_needed = 8
			let next_byte = self.source.next()?;

			if self.shift != 8 { // If mismatch is not already full, put some of the new byte into mismatch
				// (Rust requires this check since it panics when a number is completely shifted out, as on some systems a 0 is not guaranteed)
				mismatch |= next_byte >> self.shift;
			}

			// Set the remainder to what's left of the next byte (again checking to not shift the byte completely out)
			self.remainder = if self.shift == 0 { 0 } else { next_byte << (8 - self.shift) };

			Some((pattern_idx, mismatch))
		} else { // The remainder does not have enough for what pattern_idx needs.
			let mut pattern_idx = 0u64;
			// Use up remainder first
			num_of_bits_needed -= self.shift as u64;
			if self.shift != 0 {
				pattern_idx |= (self.remainder >> (8 - self.shift)) as u64;
			}

			self.shift = 0;

			// Then use as many bytes as needed
			while 8 <= num_of_bits_needed {
				let next_byte = self.source.next()?;
				pattern_idx <<= 8;
				pattern_idx |= next_byte as u64;
				num_of_bits_needed -= 8;
			}
			pattern_idx <<= num_of_bits_needed;
			let next_byte = self.source.next()?;
			// Place the tail end of pattern index in byte into the output variable (pattern_idx).
			if num_of_bits_needed != 0 {
				pattern_idx |= (next_byte >> (8 - num_of_bits_needed)) as u64;
			}

			// And the rest into mismatch
			let mut mismatch = next_byte << num_of_bits_needed;

			if let Some(next_byte) = self.source.next() {
				// Take the tail end of mismatch in byte into output variable (mismatch).
				if num_of_bits_needed != 0 {
					mismatch |= next_byte >> (8 - num_of_bits_needed);
				}
				// And the rest into remainder
				self.remainder = next_byte << num_of_bits_needed;
				self.shift = 8 - num_of_bits_needed as u8;
			} else if num_of_bits_needed != 0 {
				panic!("No more input even though we still need more bits to unpack, perhaps you forgot a flag?");
			}
			Some((pattern_idx, mismatch))
		}
	}
}