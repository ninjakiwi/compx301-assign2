//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 Packer (lib)
//!
//! Packs a stream of LZ78 pairs to a compressed binary format.
//! Can take input directly from `LZencode.rs` or `LZunpack.rs`.
//! The pack function is separate to allow easier testing
//! (used as a lib).

use crate::{mismatch_pair_parser::{ToMismatchPairFromAscii, ToMismatchPair}, helpers};
use std::io::{Read, Write, BufReader, BufWriter, BufRead};

/// Packs the stream of LZ78 pairs to the output stream by using only
/// as many bits as needed for the pattern number. The `read_ascii`
/// flag defines how input will be read.
///
/// ### Arguments
///
/// - **input** - A object that implements the read trait; data to be packed.
/// - **output** - A object that implements the write trait; used as output.
/// - **arguments** - `ProgramArguments` from `helpers.rs`.
///
/// ### Returns
///
/// The function returns no value `(())`, but the packed data is written to **output**.
pub fn pack<R: Read, W: Write>(input: R, output: W, arguments: &helpers::ProgramArguments) {
	let mut buf_reader = BufReader::with_capacity(1000, input);
	let mut buf_writer = BufWriter::with_capacity(1000, output);

	// Get max trie size if it exists
	let max_trie_size = if arguments.memory_restricted {
		// Read the max trie size value and write it straight to output
		if arguments.ascii_output {
			let mut temp = String::new();
			buf_reader.read_line(&mut temp).expect("No leading ascii line");
			let parsed = temp.trim().parse::<u64>().expect("Leading line not unsigned int");
			buf_writer.write(&helpers::u64_to_bytes(parsed)).expect("Failed write to output");
			parsed
		} else {
			// Write out the max_trie_size_data as-is
			let mut bytes = [0u8; 8];
			buf_reader.read(&mut bytes).expect("No leading 8 bytes?");
			buf_writer.write(&bytes).expect("Failed write to output");
			helpers::bytes_to_u64(&bytes)
		}
	} else {
		std::u64::MAX / 2
	};

	// Create a mismatched pairs iterator over the input stream
	let mismatch_pairs: Box<Iterator<Item=(u64, u8)>> = if arguments.ascii_output {
		Box::new(ToMismatchPairFromAscii::new(buf_reader.lines().map(|c| c.unwrap())))
	} else {
		Box::new(ToMismatchPair::new(
			buf_reader.bytes().map(|c| c.expect("Failed to read"))))
	};

	let mut shift = 0u8;
	let mut current_byte = 0u8;

	for (i, (pattern_idx, mismatch)) in mismatch_pairs.enumerate() {

		let i = i.min(max_trie_size as usize);

		// Check the pattern_idx is a valid value, (this will often catch data when a flag should/shouldn't be on)
		if i < pattern_idx as usize {
			panic!("Input data not valid LZ78, (Forgot a flag?)");
		}


		// Calculate smallest number of potentially needed bits then put pattern number in output
		let mut num_of_bits_needed = ((i + 1) as f64).log2().ceil() as usize;
		{
			// While not enough space in the current byte
			while ((8 - shift) as usize) < num_of_bits_needed {

				// Reduce num_of_bits_needed by the remaining space in the byte
				num_of_bits_needed -= (8 - shift) as usize;

				// Fill up the rest of the byte and write to output
				current_byte |= (pattern_idx >> num_of_bits_needed) as u8 & (0xFF >> shift);
				buf_writer.write(&[current_byte]).unwrap();

				// New byte; reset the current byte and the shift
				current_byte = 0;
				shift = 0;
			}

			// Now there is enough space, put in the rest of pattern_idx
			if num_of_bits_needed != 0 {

				// Fill up as much of the byte as we can
				current_byte |= ((pattern_idx as u8) << (8 - num_of_bits_needed)) >> shift;
				shift += num_of_bits_needed as u8;
			}
		}


		// Push mismatch (mismatch is only and always one byte)
		{
			let remainder = 8 - shift;

			// Place in everything that can fit
			if remainder != 0 {
				current_byte |= mismatch >> shift;
			}
			buf_writer.write(&[current_byte]).unwrap();

			// Reset and put the remainder in next byte
			current_byte = 0;
			if remainder < 8 {
				current_byte |= mismatch << remainder;
			}
			// Shift stays the same because mismatch is one byte long
		}

		// Not really necessary to be inside the loop,
		// but it saves an iteration on the more complex loop above when its true
		if shift == 8 {
			// Write current byte then reset
			buf_writer.write(&[current_byte]).unwrap();
			shift = 0;
			current_byte = 0;
		}
	}

	// Push out the final byte if there is anything inside
	if shift != 0 {
		buf_writer.write(&[current_byte]).unwrap();
	}
}
