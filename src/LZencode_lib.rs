//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 Encoder (lib)
//!
//! Encodes a stream of bytes to LZ78 pairs, one pair per line.
//! The encode function is separate to allow easier testing
//! (used as a lib).

use super::helpers;
use super::LZtrie_lib::LZTrie;
use std::io::{Write, Read, BufReader, BufWriter};

/// Encodes the input stream to the output stream in LZ78 pairs
///
/// ### Arguments
///
/// - **input** - A object that implements the read trait; data to be encoded.
/// - **output** - A object that implements the write trait; used as output.
/// - **arguments** - `ProgramArguments` from `helpers.rs`.
///
/// ### Returns
///
/// The function returns no value `(())`, but the encoded data is written to **output**.
pub fn encode<R: Read, W: Write>(input: R, output: W, arguments: &helpers::ProgramArguments) {
	let mut buf_writer = BufWriter::with_capacity(1000, output);

	// Create a buffered byte iterator from the input stream
	let buf_reader = BufReader::with_capacity(1000, input);
	let mut iterator =
		buf_reader.bytes().map(|c| c.expect("Failed to read")).peekable();

	let mut root = LZTrie::new(arguments.max_trie_size);

	if arguments.memory_restricted {
		// Write out the maximum trie size used to compress the file
		if arguments.ascii_output {
			buf_writer.write(arguments.max_trie_size.to_string().as_bytes()).unwrap();
			buf_writer.write(b"\n").unwrap();
		} else {
			buf_writer.write(&helpers::u64_to_bytes(arguments.max_trie_size)).unwrap();
		}
	}

	while iterator.peek().is_some() {
		// Push to trie, returns when end of stream is reached or the next pattern is created
		// Last pattern may already exist and may be a repeat
		let (idx, mismatch) = root.push_into_trie(&mut iterator);

		// Dump pair into the output stream in the desired format
		helpers::write_pair(&mut buf_writer, idx, mismatch, arguments.ascii_output);
	}
}