// Automated library tests
#[cfg(test)]
mod test {
	use crate::*;
	use helpers::ProgramArguments;

	fn predefined_inputs() -> Vec<Vec<u8>> {
		vec![
			b"".to_vec(), // Edge case of 0 bytes
			b"0".to_vec(), // Trivial edge case of 1 byte
			b"Testing Testing".to_vec(), // Very basic string
			b"Two blind men waited at the end of an era, contemplating beauty. They sat atop the world's highest cliff, overlooking the land and seeing nothing.".to_vec(), // Longer english string
			b"Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn.".to_vec(), // Not so english text
			b":1\nstart\ngoto 1\0".to_vec(), // Tests new line and null characters
			b"Above silence, the illuminating storms-dying storms-illuminate the silence above".to_vec(), // Ketek
			b"\t\n\0\r\x41".to_vec(), // More control characters
			"c̳̻͚̻̩̻͉̯̄̏͑̋͆̎͐ͬ͑͌́͢h̵͔͈͍͇̪̯͇̞͖͇̜͉̪̪̤̙ͧͣ̓̐̓ͤ͋͒ͥ͑̆͒̓͋̑́͞ǎ̡̮̤̤̬͚̝͙̞͎̇ͧ͆͊ͅo̴̲̺͓̖͖͉̜̟̗̮̳͉̻͉̫̯̫̍̋̿̒͌̃̂͊̏̈̏̿ͧ́ͬ̌ͥ̇̓̀͢͜s̵̵̘̹̜̝̘̺̙̻̠̱͚̤͓͚̠͙̝͕͆̿̽ͥ̃͠͡".as_bytes().to_vec(), // utf-8 nonsense
			"📼📙🌂🐂🐐🕑💚💟 🐷🐧🍶🍯💨 👽📪🌖🎑🎾 📌🕞👫🌉 👌🍮🔨🎅🔳 🌃🏆📓🏉 🕗🍈💉👼🎥 📯🎾🕣🍁📕🐥. 🔁📝🍨💣 🐕🐳🌐🔭💐🍄🐻 🕦🍄🐢👰🏪🍣💔 🐹🔱💷💷🐮🌅 💵🕝🕥🐞🌇".as_bytes().to_vec(), // Hope your text editor renders utf8 emoji's
			"🏄🌑🎒🍺💙🏣📑💶🌺🐦👻👲 🐮👂🌋🌽🍏🎂🐚🌟🐗📱🍝🌆 suspendisse dolor id 🌚🌄💒🍦👈🌱🍙👶🍍 dictum sed sapien, gravida donec quam blandit 📪🕢📏🎥🎻🎋💢 risus \
			🕕🌛🔢🔳🔜🐌🎯🍜🐡🕑🎐💶 💭🍔🍗📷🌵🌏💪📐📡🎤 pulvinar 👨🎧🎦🔊🌘👨🌳👿🐸🕖💰👟👣💁 👄🐋🔧🎸 sit facilisis tempus 🐦🍏🍀🌸 🌺🐬🎢👇🍊💚🎎👹 adipiscing justo, \
			faucibus ac feugiat 📍🔙🎷🌼 🔚🍝🍛🍓🏉🌜🌅🍛🎌🍞 🔝📄📔🍶👵📑🏣🐣🐐👗📮💀📨🐷👨💚 id imperdiet ".as_bytes().to_vec(), // Longer and mixed
			include_bytes!("../test_files/MobyDick.txt").to_vec(), // Large english text, 1.1 mb
			include_bytes!("../test_files/openvpn.exe").to_vec(), // Typical windows executable binary, 1.0 mb
		]
	}

	#[test]
	fn end_to_end_default_predefined() {
		let inputs = predefined_inputs();

		let arguments = ProgramArguments { ascii_output: false, max_trie_size: std::u64::MAX / 2, memory_restricted: false };

		for (i, input) in inputs.iter().enumerate() {
			eprintln!("{}", i);
			assert_eq!(end_to_end(&input, &arguments)[..], input[..]);
		}
	}

	#[test]
	fn end_to_end_ascii_predefined() {
		let inputs = predefined_inputs();

		let arguments = ProgramArguments { ascii_output: true, max_trie_size: std::u64::MAX / 2, memory_restricted: false };

		for (i, input) in inputs.iter().enumerate() {
			eprintln!("{}", i);
			assert_eq!(end_to_end(&input, &arguments)[..], input[..]);
		}
	}

	#[test]
	fn end_to_end_maxtrie_predefined() {
		let inputs = predefined_inputs();

		let mut arguments = ProgramArguments { ascii_output: false, max_trie_size: 1000, memory_restricted: true };

		for (i, input) in inputs.iter().enumerate() {
			eprintln!("{}", i);
			arguments.max_trie_size = (input.len() as f64).log2() as u64 / 2;
			assert_eq!(end_to_end(&input, &arguments)[..], input[..]);
		}
	}

	#[test]
	fn end_to_end_ascii_maxtrie_predefined() {
		let inputs = predefined_inputs();

		let mut arguments = ProgramArguments { ascii_output: true, max_trie_size: 1000, memory_restricted: true };

		for (i, input) in inputs.iter().enumerate() {
			eprintln!("{}", i);
			arguments.max_trie_size = (input.len() as f64).log2() as u64 / 2;
			assert_eq!(end_to_end(&input, &arguments)[..], input[..]);
		}
	}

	#[test]
	#[ignore]
	fn encode_decode_ascii_maxtrie_predefined() {
		let inputs = predefined_inputs();

		let mut arguments = ProgramArguments { ascii_output: true, max_trie_size: 1000, memory_restricted: true };

		for (i, input) in inputs.iter().enumerate() {
			eprintln!("{}", i);
			arguments.max_trie_size = (input.len() as f64).log2() as u64 / 2;
			assert_eq!(encode_decode(&input, &arguments)[..], input[..]);
		}
	}

	#[test]
	#[ignore]
	fn encode_decode_ascii_predefined() {
		let inputs = predefined_inputs();

		let mut arguments = ProgramArguments { ascii_output: true, max_trie_size: std::u64::MAX / 2, memory_restricted: false };

		for (i, input) in inputs.iter().enumerate() {
			eprintln!("{}", i);
			arguments.max_trie_size = (input.len() as f64).log2() as u64 / 2;
			assert_eq!(encode_decode(&input, &arguments)[..], input[..]);
		}
	}

	#[test]
	#[ignore]
	fn encode_decode_maxtrie_predefined() {
		let inputs = predefined_inputs();

		let mut arguments = ProgramArguments { ascii_output: false, max_trie_size: 1000, memory_restricted: true };

		for (i, input) in inputs.iter().enumerate() {
			eprintln!("{}", i);
			arguments.max_trie_size = (input.len() as f64).log2() as u64 / 2;
			assert_eq!(encode_decode(&input, &arguments)[..], input[..]);
		}
	}

	#[test]
	#[ignore]
	fn encode_decode_predefined() {
		let inputs = predefined_inputs();

		let mut arguments = ProgramArguments { ascii_output: false, max_trie_size: 1000, memory_restricted: false };

		for (i, input) in inputs.iter().enumerate() {
			eprintln!("{}", i);
			arguments.max_trie_size = (input.len() as f64).log2() as u64 / 2;
			assert_eq!(encode_decode(&input, &arguments)[..], input[..]);
		}
	}

	fn encode_decode(original: &[u8], arguments: &ProgramArguments) -> Vec<u8> {
		let mut encoded = Vec::new();
		LZencode_lib::encode(original, &mut encoded, &arguments);
		let mut output = Vec::with_capacity(original.len());
		LZdecode_lib::decode(&encoded[..], &mut output, &arguments);
		output
	}

	fn end_to_end(original: &[u8], arguments: &ProgramArguments) -> Vec<u8> {
		let mut encoded = Vec::new();
		LZencode_lib::encode(original, &mut encoded, &arguments);
		let mut packed = Vec::new();
		LZpack_lib::pack(&encoded[..], &mut packed, &arguments);
		encoded.clear();
		LZunpack_lib::unpack(&packed[..], &mut encoded, &arguments);
		packed.clear();
		let mut output = packed;
		LZdecode_lib::decode(&encoded[..], &mut output, &arguments);
		output
	}

	#[test]
	fn end_to_end_depth_test() {
		let massive = vec![96u8; 10_000_000];
		let mut arguments = ProgramArguments { ascii_output: false, max_trie_size: std::u64::MAX / 2, memory_restricted: false };
		assert_eq!(end_to_end(&massive[..], &arguments), &massive[..]);
	}

	#[test]
	fn end_to_end_breadth_test() {
		let mut arguments = ProgramArguments { ascii_output: false, max_trie_size: std::u64::MAX / 2, memory_restricted: false };
		let rand: Vec<u8> = RandomStream { seed: 1318909 }.map(|c| (c % std::u8::MAX as u64) as u8).take(500_000).collect();
		assert_eq!(end_to_end(&rand[..], &arguments), &rand[..]);
	}

	struct RandomStream {
		seed: u64,
	}

	impl Iterator for RandomStream {
		type Item = u64;
		fn next(&mut self) -> Option<u64> {
			self.seed = random(self.seed);
			Some(self.seed)
		}
	}

	fn random(mut x: u64) -> u64 {
		x ^= x << 21;
		x ^= x >> 35;
		x ^= x << 4;
		return x;
	}
}