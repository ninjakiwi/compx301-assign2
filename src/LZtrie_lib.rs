//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # Trie Library
//!
//! Provides a trie structure useful for encoding bytes to LZ78
//!
//! # Structs
//!
//! - **PushError** - Errors that can occur when pushing to the trie
//! - **LZTrie** - The trie itself, use this to create a trie and push to it.
//! - **LZTrieNode** - Used to store pattern and mismatch pairs within the trie.
//! - **BTreeNode** - A binary tree containing a trie node.

use ::std::{error::Error, fmt};

#[derive(Debug, PartialEq)]
pub enum PushError {
	DuplicateMismatch,
	PatternIdxError,
}

impl Error for PushError {}

impl fmt::Display for PushError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		use self::PushError::*;
		match self {
			&DuplicateMismatch => write!(f, "The mismatch already exists as a child"),
			&PatternIdxError => write!(f, "PatternsIndicies can not be repeated, \
				therefore the new idx can not be smaller than the current node"),
		}
	}
}

/// The Trie itself
pub struct LZTrie {
	root: LZTrieNode,
	current_max_idx: u64,
	max_trie_size: u64,
}

impl LZTrie {
	/// Initialize a new trie.
	pub fn new(max_trie_size: u64) -> LZTrie {
		let root = LZTrieNode::new(0, 0);
		LZTrie {
			root,
			current_max_idx: 0,
			max_trie_size,
		}
	}

	/// Push onto the trie until a new pattern is found or the iter runs out,
	/// then return the pattern and mismatch.
	pub fn push_into_trie<I>(&mut self, input_data_iter: &mut I) -> (u64, u8)
		where I: Iterator<Item=u8> {
		let mut current_node = &mut self.root;
		let mut old_idx = 0;

		for byte in input_data_iter {
			if current_node.contains(byte) {
				old_idx = current_node.pattern_idx;
				current_node = current_node.get_child_mut(byte).unwrap();
			} else {
				if self.current_max_idx < self.max_trie_size {
					self.current_max_idx += 1;
					let new_node = LZTrieNode::new(self.current_max_idx, byte);
					current_node.push(new_node).expect("Push to trie failed"); // More detail will show from the internal error
				}
				// Return the parent's idx (current_node is the parent)
				return (current_node.pattern_idx, byte);
			}
		}

		(old_idx, current_node.mismatch)
	}
}

/// Node for trie
#[derive(Debug)]
struct LZTrieNode {
	pattern_idx: u64,
	mismatch: u8,
	children: BST,
}

impl PartialEq for LZTrieNode {
	fn eq(&self, other: &Self) -> bool {
		self.pattern_idx == other.pattern_idx && self.mismatch == other.mismatch
	}
}

impl LZTrieNode {
	/// Creates a new LZTrie element, let pattern_num = 0 be root
	pub fn new(pattern_idx: u64, mismatch: u8) -> LZTrieNode {
		return LZTrieNode {
			pattern_idx,
			mismatch,
			children: BST::new(),
		};
	}

	/// Push a LZTrieNode as a child to this node (adds to B-tree)
	pub fn push(&mut self, lz_pair: LZTrieNode) -> Result<(), PushError> {
		if lz_pair.pattern_idx <= self.pattern_idx {
			return Err(PushError::PatternIdxError);
		}

		self.children.insert(lz_pair)
//		if let Some(ref mut child) = self.children {
//			child.insert(lz_pair)
//		} else {
//			self.children = Some(Box::new(BTreeNode::new(lz_pair)));
//			Ok(())
//		}
	}

	/// Returns true if the mismatched byte is a direct child
	pub fn contains(&self, mismatched_byte: u8) -> bool {
		self.children.find_child(mismatched_byte).is_some()
	}

	/// Returns a mutable reference the child with the specified mismatched byte
	pub fn get_child_mut(&mut self, mismatched_byte: u8) -> Option<&mut LZTrieNode> {
		self.children.find_child_mut(mismatched_byte)
	}
}

/// A binary tree used to order and search a trie node's children
#[derive(Debug)]
struct BST {
	root: Option<Box<BTreeNode>>,
}

impl BST {
	pub fn new() -> BST {
		BST { root: None }
	}

	pub fn insert(&mut self, new_node: LZTrieNode) -> Result<(), PushError> {
		if self.root.is_none() {
			self.root = Some(Box::new(BTreeNode::new(new_node)));
		} else {
			self.root = Some(BTreeNode::insert(self.root.take().unwrap(), new_node)?);
		}
		Ok(())
	}

	pub fn find_child_mut(&mut self, mismatched_byte: u8) -> Option<&mut LZTrieNode> {
		self.root.as_mut()?.find_mut(mismatched_byte)
			.map(|c| &mut c.data)
	}

	pub fn find_child(&self, mismatched_byte: u8) -> Option<&LZTrieNode> {
		self.root.as_ref()?.find(mismatched_byte)
			.map(|c| &c.data)
	}
}

#[derive(Debug)]
struct BTreeNode {
	data: LZTrieNode,
	height: isize,
	left: Option<Box<BTreeNode>>,
	right: Option<Box<BTreeNode>>,
}

impl BTreeNode {
	/// Initialize a new node
	pub fn new(data: LZTrieNode) -> BTreeNode {
		BTreeNode { data, left: None, height: 1, right: None }
	}

	/// Insert a new node into current_node, then returns the node.
	pub fn insert(mut current_node: Box<BTreeNode>, new_node: LZTrieNode) -> Result<Box<BTreeNode>, PushError> {

		// Store mismatch as it is needed after new_node is moved
		let new_mismatch = new_node.mismatch;

		if current_node.data.mismatch == new_mismatch {
			return Err(PushError::DuplicateMismatch);
		}

		let (target_node_value, target_node) =
			if new_mismatch < current_node.data.mismatch {
				(current_node.left.take(), &mut current_node.left)
			} else {
				(current_node.right.take(), &mut current_node.right)
			};

		match target_node_value {
			Some(child) => {
				*target_node = Some(Self::insert(child, new_node)?);
			}
			None => {
				*target_node = Some(Box::new(BTreeNode::new(new_node)));
			}
		}

		current_node.update_height();

		let balance = current_node.balance();

		if balance > 1 {
			let left_child_mismatch = current_node.left.as_ref().unwrap().data.mismatch;
			if new_mismatch < left_child_mismatch {  // Left Left
				return Ok(BTreeNode::right_rotate(current_node));
			} else if new_mismatch > left_child_mismatch { // Left Right
				current_node.left = Some(BTreeNode::left_rotate(current_node.left.take().unwrap()));
				return Ok(BTreeNode::right_rotate(current_node));
			}
		} else if balance < -1 {
			let right_child_mismatch = current_node.right.as_ref().unwrap().data.mismatch;
			if new_mismatch > right_child_mismatch {  // Right Right
				return Ok(BTreeNode::left_rotate(current_node));
			} else if new_mismatch < right_child_mismatch { // Right Left
				current_node.right = Some(BTreeNode::right_rotate(current_node.right.take().unwrap()));
				return Ok(BTreeNode::left_rotate(current_node));
			}
		}

		// No rotations needed
		Ok(current_node)
	}

	/// Find a node with the provided mismatch
	pub fn find(&self, mismatch: u8) -> Option<&BTreeNode> {
		if self.data.mismatch == mismatch {
			return Some(self);
		}
		let mut cur = self;
		while let Some(list) = if mismatch < cur.data.mismatch { &cur.left } else { &cur.right } {
			if list.data.mismatch == mismatch { return Some(list); }
			cur = list;
		}

		None
	}

	/// Get a mutable reference to a node with the provided mismatch
	pub fn find_mut(&mut self, mismatch: u8) -> Option<&mut BTreeNode> {
		if self.data.mismatch == mismatch {
			return Some(self);
		}
		let mut cur = self;
		while let Some(list) = if mismatch < cur.data.mismatch { &mut cur.left } else { &mut cur.right } {
			if list.data.mismatch == mismatch { return Some(list); }
			cur = list;
		}

		None
	}

	pub fn right_rotate(mut y: Box<BTreeNode>) -> Box<BTreeNode> {
		let mut x = y.left.take().expect("Tried to rotate right without left child");

		y.left = x.right.take();
		y.update_height();

		x.right = Some(y);
		x.update_height();

		x
	}

	pub fn left_rotate(mut x: Box<BTreeNode>) -> Box<BTreeNode> {
		let mut y = x.right.take().expect("Tried to rotate left without right child");

		x.right = y.left.take();
		x.update_height();

		y.left = Some(x);
		y.update_height();

		y
	}

	#[inline]
	fn update_height(&mut self) {
		self.height = std::cmp::max(self.left.as_ref().map(|c| c.height).unwrap_or(0),
		                            self.right.as_ref().map(|c| c.height).unwrap_or(0)) + 1;
	}

	pub fn balance(&self) -> isize {
		(self.left.as_ref().map(|c| c.height).unwrap_or(0) - self.right.as_ref().map(|c| c.height).unwrap_or(0))
	}
}

#[cfg(test)]
mod trie_node_test {
	use super::LZTrieNode;

	#[test]
	fn initialize_test() {
		let mut new_trie_node = LZTrieNode::new(12, 16);
		assert!(new_trie_node.get_child_mut('a' as u8).is_none());
	}
}

#[cfg(test)]
mod trie_test {
	use std;
	use super::LZTrie;

	#[test]
	fn push_encode_test() {
		let data = b"aaaabbbbaaaa"; // string to compress

		let mut iterator = data.iter().cloned();

		let mut new_trie = LZTrie::new(std::u64::MAX / 2);

		// Get pairs
		assert_eq!(new_trie.push_into_trie(&mut iterator), (0, 'a' as u8)); // <0, a>
		assert_eq!(new_trie.push_into_trie(&mut iterator), (1, 'a' as u8)); // <1, a>
		assert_eq!(new_trie.push_into_trie(&mut iterator), (1, 'b' as u8)); // <1, b>
		assert_eq!(new_trie.push_into_trie(&mut iterator), (0, 'b' as u8)); // <0, b>
		assert_eq!(new_trie.push_into_trie(&mut iterator), (4, 'b' as u8)); // <4, b>
		assert_eq!(new_trie.push_into_trie(&mut iterator), (2, 'a' as u8)); // <2, a>
		assert_eq!(new_trie.push_into_trie(&mut iterator), (0, 'a' as u8)); // <0, a>
		assert_eq!(new_trie.push_into_trie(&mut iterator), (0, 0)); // done (root node)
	}
}