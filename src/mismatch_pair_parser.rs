//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 pair parsers
//!
//! This file contains a couple of handy structs used in our LZ78 files.
//!
//! # Structs
//!
//! - **ToMismatchPair** - An iterator that gathers bytes into LZ78 pairs
//! - **ToMismatchPairFromAscii** - An iterator that gathers ascii into LZ78 pairs

/// Provides a iterator data type to take a iterator of bytes and turn it
/// into a iterator over LZ78 pairs
pub struct ToMismatchPair<I: Iterator<Item=u8>> {
	source: I,
}

impl<I: Iterator<Item=u8>> ToMismatchPair<I> {
	pub fn new(source: I) -> Self {
		Self {
			source,
		}
	}
}

impl<I: Iterator<Item=u8>> Iterator for ToMismatchPair<I> {
	type Item = (u64, u8);
	fn next(&mut self) -> Option<(u64, u8)> {
		let mut pattern_idx = 0u64;

		// Gather next 8 bytes for pattern number (u64)
		pattern_idx |= self.source.next()? as u64; // Return None if there is no next byte.
		for _ in 0..7 {
			pattern_idx <<= 8;
			pattern_idx |= self.source.next().expect("Incomplete input stream, perhaps you forgot a flag?") as u64;
		}

		// Get the (8 bit) mismatch
		let mismatch = self.source.next().expect("Incomplete input stream, perhaps you forgot a flag?");

		// Throw away the useless new line character
		self.source.next().expect("Somehow the useless new line escaped! Perhaps you forgot a flag?");

		Some((pattern_idx, mismatch))
	}
}

/// Provides a iterator data type to take a iterator of Strings and turn
/// it into a iterator over LZ78 pairs
pub struct ToMismatchPairFromAscii<I: Iterator<Item=String>> {
	lines: I,
}

impl<I: Iterator<Item=String>> ToMismatchPairFromAscii<I> {
	pub fn new(lines: I) -> Self {
		Self {
			lines,
		}
	}
}

impl<I: Iterator<Item=String>> Iterator for ToMismatchPairFromAscii<I> {
	type Item = (u64, u8);
	fn next(&mut self) -> Option<(u64, u8)> {

		// Get the next pair
		let next = self.lines.next()?;
		let mut split = next.split(' ');

		// Separate pattern and mismatch
		let pattern_idx = split.next().expect("Incomplete input stream, perhaps you forgot a flag?")
			.parse::<u64>().expect("Input not ascii integer");
		let mismatch = split.next().expect("Incomplete input stream, perhaps you forgot a flag?")
			.parse::<u8>().expect("Input not ascii integer");

		Some((pattern_idx, mismatch))
	}
}