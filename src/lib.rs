#![allow(non_snake_case)]

// Link together all the separate files
pub mod helpers;
pub mod LZtrie_lib;
pub mod LZencode_lib;
pub mod LZdecode_lib;
pub mod LZpack_lib;
pub mod LZunpack_lib;
pub mod mismatch_pair_parser;

mod int_tests;