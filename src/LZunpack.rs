//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 Unpacker
//!
//! Unpacks a stream of bytes to LZ78 pairs, one pair per line.
//! Can take input from `LZpack.rs`.
//! (Same output as `LZencode.rs`)
//! The unpack function is separate to allow easier testing
//! (use as a lib).
//!
//! # Input
//!
//! * A byte stream through standard input to be the data to be unpacked.
//! * (Optional) "-a" argument to specify the output to be read as ascii
//! rather than integer values.
//!
//! # Output
//!
//! Returns the paired bytes line by line via standard out, each line formated with
//! the pattern then the mismatched byte.

extern crate lz78;

use lz78::{helpers, LZunpack_lib::unpack};
use std::io::{stdin, stdout};

fn main() {
	let arguments = helpers::parse_arguments(std::env::args());
	unpack(stdin().lock(), stdout().lock(), &arguments);
}