//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 Packer
//!
//! Packs a stream of LZ78 pairs to a compressed binary format.
//! Can take input directly from `LZencode.rs` or `LZunpack.rs`.
//! The pack function is separate to allow easier testing
//! (use as a lib).
//!
//! # Input
//!
//! * A byte stream through standard input to be the data to be packed.
//! * (Optional) "-a" argument to specify the output to be read in ascii form.
//!
//! # Output
//!
//! A compressed LZ78 pair byte stream via standard out.

extern crate lz78;

use lz78::{helpers, LZpack_lib::pack};
use std::io::{stdin, stdout};

fn main() {
	let arguments = helpers::parse_arguments(std::env::args());
	pack(stdin().lock(), stdout().lock(), &arguments);
}