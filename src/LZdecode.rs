//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 Decoder
//!
//! Decodes a stream of LZ78 pairs to the original data.
//! Can take input directly from `LZunpack.rs` and `LZencode.rs`.
//! The decode function is separate to allow easier testing
//! (used as a lib).
//!
//! # Input
//!
//! * A byte stream through standard input to be the data to be decoded.
//! * (Optional) "-a" argument to specify the input to be encoded in ascii form.
//!
//! # Output
//!
//! Returns the original, decoded data via standard out.

extern crate lz78;

use lz78::{helpers, LZdecode_lib::decode};
use std::io::{stdin, stdout};

fn main() {
	let arguments = helpers::parse_arguments(std::env::args());
	decode(stdin().lock(), stdout().lock(), &arguments);
}