//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 Decoder (lib)
//!
//! Decodes a stream of LZ78 pairs to the original data.
//! Can take input directly from `LZunpack.rs` and `LZencode.rs`.
//! The decode function is separate to allow easier testing
//! (used as a lib).

use crate::{mismatch_pair_parser::{ToMismatchPairFromAscii, ToMismatchPair}, helpers};
use std::io::{Write, Read, BufReader, BufWriter, BufRead};

/// Decode the input stream specified by `LZencode.rs`'s output and output the original data
///
/// ### Arguments
///
/// - **input** - A object that implements the read trait; data to be encoded.
/// - **output** - A object that implements the write trait; used as output.
/// - **arguments** - `ProgramArguments` from `helpers.rs`.
///
/// ### Returns
///
/// The function returns no value `(())`, but the encoded data is written to **output**.
pub fn decode<R: Read, W: Write>(input: R, output: W, arguments: &helpers::ProgramArguments) {
	let mut buf_writer = BufWriter::with_capacity(1000, output);

	// Create a buffered iterator over the LZ78 pairs from standard input.
	let mut buf_reader = BufReader::with_capacity(1000, input);

	let max_trie_size = if arguments.memory_restricted {
		// Parse the leading 8 bytes for the maximum trie size
		 if arguments.ascii_output {
			let mut temp = String::new();
			buf_reader.read_line(&mut temp).expect("No leading ascii line");
			temp.trim().parse::<u64>().expect("Leading line not unsigned int")
		} else {
			let mut bytes = [0u8; 8];
			buf_reader.read(&mut bytes).expect("No leading 8 bytes?");
			helpers::bytes_to_u64(&bytes)
		}
	} else {
		std::u64::MAX / 2
	};

	let mismatch_pairs: Box<Iterator<Item=(u64, u8)>> = if arguments.ascii_output {
		Box::new(ToMismatchPairFromAscii::new(buf_reader.lines().map(|c| c.unwrap())))
	} else {
		Box::new(ToMismatchPair::new(
			buf_reader.bytes().map(|c| c.expect("Failed to read"))))
	};

	// Create a dictionary for discovered patterns
	let mut pattern_collection: Vec<Vec<u8>> = vec![Vec::new()];

	for (pattern_idx, mismatch) in mismatch_pairs {
		// Clone existing pattern
		let mut original = pattern_collection.get(pattern_idx as usize)
			.expect("Pattern idx not inside trie yet").clone();

		// Add the new mismatch
		original.push(mismatch);

		// Write new pattern to output and add to collection
		buf_writer.write(&original).unwrap();

		// Smaller or equals to compensate for the empty vector
		// Note: Do not add or subtract from max_trie_size, it could be u64::MAX or 0
		if pattern_collection.len() <= max_trie_size as usize {
			pattern_collection.push(original);
		}
	}
}