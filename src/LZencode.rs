//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 Encoder
//!
//! Encodes a stream of bytes to LZ78 pairs, one pair per line.
//! The encode function is separate to allow easier testing
//! (used as a lib).
//!
//! # Input
//!
//! * A byte stream through standard input to be the data to be encoded.
//! * (Optional) "-a" argument to specify the output to be printed in ascii form.
//!
//! # Output
//!
//! Returns the paired bytes line by line via standard out, each line formated with
//! the pattern then the mismatched byte.

extern crate lz78;

use lz78::{helpers, LZencode_lib::encode};
use std::io::{stdin, stdout};

fn main() {
	let arguments = helpers::parse_arguments(std::env::args());
	encode(stdin().lock(), stdout().lock(), &arguments);
}