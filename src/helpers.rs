//! # Authors
//!
//! * Daniel Martin - 1349779
//! * Rex Pan - 1318909
//!
//! # LZ78 Helpers
//!
//! This file contains a bunch of handy functions used in our LZ78 files.
//!
//! # Functions
//!
//! - **u64_to_bytes** - Turns an unsigned 64 bit integer into a byte array.
//! - **bytes_to_u64** - Turns a byte array into an unsigned 64 bit integer.
//! - **write_pair** - Writes a LZ78 pair to the provided output stream.
//!
//! # Structs
//!
//! - **ProgramArguments** - Arguments/Flags.

use std::io::Write;

/// Returns a byte array 8 bytes long representing the given 64 bit, unsigned integer
pub fn u64_to_bytes(input: u64) -> [u8; 8] {
	[
		(input >> 56) as u8,
		((input >> 48) & 0xFF) as u8,
		((input >> 40) & 0xFF) as u8,
		((input >> 32) & 0xFF) as u8,
		((input >> 24) & 0xFF) as u8,
		((input >> 16) & 0xFF) as u8,
		((input >> 8) & 0xFF) as u8,
		(input & 0xFF) as u8,
	]
}

/// Converts output from u64_to_bytes back int u64
pub fn bytes_to_u64(input: &[u8;8]) -> u64 {
	let mut max_trie_size = 0;
	for i in 0..8 {
		max_trie_size <<= 8;
		max_trie_size |= input[i] as u64;
	}
	max_trie_size
}

/// Writes a LZ78 pair to the given output stream, format dependent on the `write_ascii` flag
pub fn write_pair<W: Write>(writer: &mut W, pattern_idx: u64, mismatch: u8, write_ascii: bool) {
	if write_ascii {
		writer.write(pattern_idx.to_string().as_bytes()).expect("Failed to write to output");
		writer.write(b" ").expect("Failed to write to output");
		writer.write(mismatch.to_string().as_bytes()).expect("Failed to write to output");
		writer.write(b"\n").expect("Failed to write to output");
	} else {
		writer.write(&u64_to_bytes(pattern_idx)).expect("Failed to write to output");
		writer.write(&[mismatch]).expect("Failed to write to output");
		writer.write(b"\n").expect("Failed to write to output");
	}
}

#[derive(Debug)]
pub struct ProgramArguments {
	pub ascii_output: bool,
	pub max_trie_size: u64,
	pub memory_restricted: bool,
}

/// Parses program arguments to the ProgramArguments struct
pub fn parse_arguments(args: std::env::Args) -> ProgramArguments {
	//let args: Vec<String> = args.skip(1).collect();
	let mut output = ProgramArguments {ascii_output: false, max_trie_size: std::u64::MAX / 2, memory_restricted: false}; // Divide by 2 just to avoid edge cases
	for arg in args.skip(1) {
		if arg == "-a" {
			output.ascii_output = true;
		} else if arg.starts_with("--maxtrie") {
			output.max_trie_size = arg.split('=').nth(1).expect("No equal sign?").parse::<u64>().expect("Not unsigned integer?");
			output.memory_restricted = true;
		} else if arg == "-m" {
			output.memory_restricted = true;
		} else {
			eprintln!("The argument \"{}\" was not recognized, ignoring.", arg);
		}
	}
	output
}

#[cfg(test)]
mod helper_tests {
    use crate::helpers::{bytes_to_u64, u64_to_bytes};

    #[test]
    fn does_u64_to_bytes_work() {
        assert_eq!(u64_to_bytes(1), [0, 0, 0, 0, 0, 0, 0, 1]);
        assert_eq!(u64_to_bytes(35), [0, 0, 0, 0, 0, 0, 0, 35]);
        assert_eq!(u64_to_bytes(0xFF), [0, 0, 0, 0, 0, 0, 0, 0xFF]);
        assert_eq!(u64_to_bytes(0x100), [0, 0, 0, 0, 0, 0, 1, 0]);
        assert_eq!(u64_to_bytes(0x10000), [0, 0, 0, 0, 0, 1, 0, 0]);
        assert_eq!(u64_to_bytes(0x1000001), [0, 0, 0, 0, 1, 0, 0, 0x1]);
        assert_eq!(u64_to_bytes(0x100000051), [0, 0, 0, 1, 0, 0, 0, 0x51]);
        assert_eq!(u64_to_bytes(0x10100000051), [0, 0, 1, 1, 0, 0, 0, 0x51]);
        assert_eq!(u64_to_bytes(0x1000100000051), [0, 1, 0, 1, 0, 0, 0, 0x51]);
        assert_eq!(u64_to_bytes(0x101000100000051), [1, 1, 0, 1, 0, 0, 0, 0x51]);
        assert_eq!(u64_to_bytes(0xFFFFFFFFFFFFFFFF), [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
    }

    #[test]
    fn do_all_conversions_work() {
        assert_eq!(bytes_to_u64(&u64_to_bytes(1)), 1);
        assert_eq!(bytes_to_u64(&u64_to_bytes(0xFF)), 0xFF);
        assert_eq!(bytes_to_u64(&u64_to_bytes(0x10000)), 0x10000);
        assert_eq!(bytes_to_u64(&u64_to_bytes(0x100000051)), 0x100000051);
        assert_eq!(bytes_to_u64(&u64_to_bytes(0x1000100000051)), 0x1000100000051);
        assert_eq!(bytes_to_u64(&u64_to_bytes(0xFFFFFFFFFFFFFFFF)), 0xFFFFFFFFFFFFFFFF);

        assert_eq!(u64_to_bytes(bytes_to_u64(&[0, 0, 0, 0, 0, 0, 0, 1])), [0, 0, 0, 0, 0, 0, 0, 1]);
        assert_eq!(u64_to_bytes(bytes_to_u64(&[0, 0, 0, 0, 0, 0, 0, 0xFF])), [0, 0, 0, 0, 0, 0, 0, 0xFF]);
        assert_eq!(u64_to_bytes(bytes_to_u64(&[0, 0, 0, 0, 0, 1, 0, 0])), [0, 0, 0, 0, 0, 1, 0, 0]);
        assert_eq!(u64_to_bytes(bytes_to_u64(&[0, 0, 0, 1, 0, 0, 0, 0x51])), [0, 0, 0, 1, 0, 0, 0, 0x51]);
        assert_eq!(u64_to_bytes(bytes_to_u64(&[0, 1, 0, 1, 0, 0, 0, 0x51])), [0, 1, 0, 1, 0, 0, 0, 0x51]);
        assert_eq!(u64_to_bytes(bytes_to_u64(&[0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF])), [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
    }
}
