# COMPX301 Assignment 2

Second group assignment for COMPX301

A LZ78 encoder, packer, unpacker and decoder written in rust, following the specifications
from the [assignment instructions](https://www.cs.waikato.ac.nz/~tcs/COMPX301/assign2-2019.html).

In this project:
 * `Intermediate` refers to the file format output by `lzencode` and `lzunpack`.
 * `Initial` refers to *initial* data (input to `lzencode` and output by `lzdecode`).
 * `Packed` refers to the fully packed data output by `lzpack`.

# Authors
 - Daniel Martin - 1349779
 - Rex Pan - 1318909

# In This Project

### Binaries

 - `src/LZencode.rs` *(lzencode)* - Encodes a byte stream into LZ78 pairs (intermediate format).
 - `src/LZdecode.rs` *(lzdecode)* - Decodes LZ78 pairs back to the original byte stream.
 - `src/LZpack.rs` *(lzpack)* - Packs LZ78 pairs using as little space as possible (packed format).
 - `src/LZunpack.rs` *(lzunpack)* - Unpacks LZ78 pairs (intermediate format) from the packed format.
 
 The binaries each only consists of a few lines calling the library functions below to do the actual work, 
 this is done to make automated testing easier.  

### Libraries

 - `src/lib.rs` *(lz78)* - Links together all the separate library files.
 - `src/LZtrie_lib.rs` - Provides a trie and an unbalanced BST used to store each Trie nodes children.
 - `src/helpers.rs` - Provides handy functions/structs for various common tasks.
 - `src/mismatch_pair_parser.rs` - Provides iterators that convert the bytes in the `Intermediate` format into usable rust data types.
 - `src/LZencode_lib.rs` - Contains the actual code for encoding data.
 - `src/LZdecode_lib.rs` - Contains the actual code for decoding data.
 - `src/LZpack_lib.rs` - Contains the actual code for packing data.
 - `src/LZunpack_lib.rs` - Contains the actual code for unpacking data.
 - `src/int_tests.rs` - Comprehensive integration tests for the library.

# The `Intermediate` format

### Output Types

Because the assignment is somewhat unclear, the intermediate format comes in two formats:

 - **Binary** (Default) - The pairs are separated by an unused newline byte as assignment specifications. Each pair is represented as an
 unsigned 64 bit integer for the pattern directly followed by an unsigned 8 bit integer for the mismatched byte,
 meaning each pair takes exactly 10 bytes (8 + 1 + 1). 
 This method is preferred as it does not require ascii parsing/conversion.
 - **ASCII** (with `-a` flag) - The pairs are stored line by line as ascii with index and mismatch separated by a space
 So the 64 bit pattern is stored as ascii integers rather than bytes and same for the mismatched byte.

We provide the ascii format as it is human readable and the binary as it is faster to compute at every stage.
Because of the different storage formats, the file size will differ; but both intermediate formats get
packed the same (Just remember the `-a` flag for `lzpack` and `lzdecode` when the intermediate is in the ascii format).

> **Note:** The mismatched character in it's ascii format displays as it's ascii character code to make the output
	easy to read even for binary initial files

### Large Files

Because of large memory requirement required even for relatively small files (e.g. `13.7mb` windows executable file
takes around `100mb` of memory, `55mb` takes `365mb` of memory), and the diminishing returns with larger trie sizes,
we put in a limiting option for large files and/or low memory situations.

> **Warning:** Using this option *changes* the both the intermediate and packed file format. Files that were initially encoded using the `--maxtrie`
    option requires all other binaries to use the `-m` flag when parsing as 8 extra bytes are added to the start of each file.

Set the *Max Trie Size* with `--maxtrie=x` and indicate the usage of the limit on all other binaries using the `-m` flag.
You can mix this with the `-a` flag to get the limit stored in the output file on the first line in ascii. 

> **Example:** Set `--maxtrie=2000000` (2 million) to cap memory use to `100mb`. If run with `--maxtrie=0`, the packed output
	will be the same as the input file with a 8 empty bytes at the start.
	
> **Note:** The code that implements this functionality can also be used to improve the pack/compression ratio by setting `--maxtrie`
to equal the maximum pattern index that is actually used. But because of the stream nature of `stdin` and `stdout`, 
it would require storing the entire output of lzencode in memory, or into a temporary file, before the maximum pattern index is known
 and therefore before any data is passed to lzpack, slowing down the compression processes. 

# How To Use

This code was compiled during development with the latest stable release of rust - `rustc 1.34.1 (2019-04-24)`,
 though it should work with any other version of rust that supports the 2018 edition.

To compile the binaries (`src/LZencode.rs`, `src/LZdecode.rs`, `src/LZpack.rs` and `src/LZunpack.rs`) use cargo
which will organize dependencies and link them.

```
$cargo build --release
```

The compiled binaries will be in `target/release/`.

It is recommended to pipe the output of `lzencode` to `lzpack` to create a packed file,
and `lzunpack` to `lzdecode` to return the data to it's original state.

### Arguments

 * `-a` - Indicates that the intermediate file is in ascii format.
 * `--maxtrie=x` - Sets the maximum number of trie nodes to `x`.
 (only applicable in `lzencode`, all other binaries will interpret this as setting the `-m` flag) defaults to the max size of a
 unsigned 63 bit integer (practically unlimited).
  If this argument is set when encoding, 
  `-m` must be passed to all other binaries parsing the encoded file.
 * `-m` - Indicates that the input data has been encoded with the `maxtrie` setting 

### Example

To compress `input.txt`, into `squashed.lzp`, navigate to `target/release/` and run the binaries:

```
$cd target/release/
$cat input.txt | ./lzencode | ./lzpack > squashed.lzpacked
```

To uncompressed `squashed.lzp` to `output.txt`:

```
$cat squashed.lzpacked | ./lzunpack | ./lzdecode > output.txt
```

If you want a ascii readable intermediate file (output of `lzencode` or `lzunpack`):

```
$cat input.txt | ./lzencode -a > intermediate.lzencoded
```

Using maxtrie to limit memory usage to `100mb`, while writing out a intermediate ascii file. 
Then decompressing as fast as possible by not using the `-a` flag.
```
$cat input.txt | ./lzencode -a --maxtrie=2000000 > intermediate_ascii.lzencoded
$cat intermediate_ascii.lzencoded | ./lzpack -a -m > input.packed
$cat input.packed | ./lzunpack -m | ./lzdecode -m > decompressed_input.txt
```
To run the comprehensive automated tests: 
```
cargo test --release
```

# Output

The output of `lzencode` and `lzunpack` are the lines of LZ78 pairs representing the
initial data. The output of `lzpack` is our attempt at the smallest representation of
the initial data. `lzdecode` should output the initial data.

So the flow of data should look like this (input -> function -> output):

| Input           | Function   | Output         |
| :-------------- | :--------- | :------------- |
| Initial         | `lzencode` | Intermediate   |
| Intermediate    | `lzdecode` | Initial        |
| Intermediate    | `lzpack`   | Packed         |
| Packed          | `lzunpack` | Intermediate   |